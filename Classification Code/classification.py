## First of all, thank you Codebasics and StackOverFlow!

## TASK1: Collect lots of data and up to you what you want to collect about. âœ“

    ## I Collected some Turkish Twitch Streamer images. (Pqueen, members of Rekkitz)
        #Note: This project is only for lesson.

## TASK2: Make data cleaning and use two eyes of your images to recognize. (Cascade Classifier)  âœ“


import numpy as np
import cv2 
import matplotlib.pyplot as plt


## READ

img = cv2.imread('./test_images/zade.jpg') #We reading jpg file as image
print(img.shape)

plt.imshow(img)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #We colored it as Gray
print(gray.shape)
plt.imshow(gray, cmap="gray")

## DETECT THE EYES

face_cascade = cv2.CascadeClassifier('./opencv/haarcascades/haarcascade_frontalface_default.xml') #Uploaded
eye_cascade = cv2.CascadeClassifier('./opencv/haarcascades/haarcascade_eye.xml') #Uploaded

faces = face_cascade.detectMultiScale(gray, 1.3,5)
print(faces) # X , Y , Height , Weight

(x,y,w,h) = faces[0] #To detect scales of the face

#To draw rectangle around that face

face_img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
plt.imshow(face_img)

#To detect the eyes of picture (from OpenCV Documentation)

cv2.destroyAllWindows()
for (x,y,w,h) in faces:
    face_img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = face_img[y:y+h, x:x+w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
        cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,255,255),2)
        
# -- # 

plt.figure()
plt.imshow(face_img, cmap="gray")
plt.show()

#To crop everything except inside of red rectangle.

plt.imshow(roi_color, cmap="gray")

#Let's create a function to detect eyes of every single picture that we had.

def get_cropped_image_if_2_eyes(image_path):
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        if len(eyes) >= 2:
            return roi_color
        
original_image = cv2.imread('./test_images/zade.jpg')
plt.imshow(original_image)

cropped_image = get_cropped_image_if_2_eyes('./test_images/zade.jpg')
plt.imshow(cropped_image)

# org_image_obstructed = cv2.imread('./test_images/images.jpg') âœ“
# cropped_image2 = get_cropped_image_if_2_eyes('./test_images/images.jpg') âœ“
# plt.imshow(cropped_image2) âœ“



#after we create that function, let's a folder to cropped images

path_to_data = "./dataset/"
path_to_cr_data = "./dataset/cropped/"

import os 
img_dirs = []
for entry in os.scandir(path_to_data):
    if entry.is_dir():
        img_dirs.append(entry.path)

print(img_dirs)

import shutil
if os.path.exists(path_to_cr_data):
    shutil.rmtree(path_to_cr_data)
os.mkdir(path_to_cr_data)

#We'll create a dictionary that called celebrity_file_names_dict and cropped_image_dirs to upload images that cropped


cropped_image_dirs = []
celebrity_file_names_dict = {}

for img_dir in img_dirs:
    count = 1
    celebrity_name = img_dir.split('/')[-1] #[-1] last word
    print(celebrity_name)
    
    celebrity_file_names_dict[celebrity_name] = []
    
    for entry in os.scandir(img_dir):
        roi_color = get_cropped_image_if_2_eyes(entry.path)
        if roi_color is not None:
            cropped_folder = path_to_cr_data + celebrity_name
            if not os.path.exists(cropped_folder):
                os.makedirs(cropped_folder)
                cropped_image_dirs.append(cropped_folder)
                print("Generating cropped images in folder",cropped_folder)
                
                
            cropped_file_name = celebrity_name + str(count) + ".png" # png files that i will upload images
            cropped_file_path = cropped_folder + "/" + cropped_file_name
            
            cv2.imwrite(cropped_file_path, roi_color)
            celebrity_file_names_dict[celebrity_name].append(cropped_file_path)
            count += 1
            

#Wavelet Transform: -F U F 
import pywt
def w2d(img, mode='haar', level=1):
    imArray = img
    #DATATYPE CONVERSIONS
    #Convert to grayscale
    imArray = cv2.cvtColor(imArray,cv2.COLOR_RGB2GRAY)
    #Convert to float
    imArray = np.float32(imArray)
    imArray /= 255;
    #Compute coefficients
    coeffs =pywt.wavedec2(imArray, mode, level=level)
    
    #Process Coefficients
    coeffs_H = list(coeffs)
    coeffs_H[0] *= 0;
    
    #Reconstruction
    imArray_H = pywt.waverec2(coeffs_H,mode);
    imArray_H *= 255;
    imArray_H = np.uint8(imArray_H)
    
    return imArray_H

im_har = w2d(cropped_image,'db1',5)
plt.imshow(im_har, cmap='gray')

#Wavelet Transform is completed (-f U f)
                                # 0 U 1
#Also Fourier Transform will take a complex signal and it will return you the basic signals.
print(celebrity_file_names_dict)

#We'll create a class dictionary to stack all names of Turkish Twitch Streamers.
class_dict = {}
count = 0
for celebrity_name in celebrity_file_names_dict.keys():
    class_dict[celebrity_name] = count
    count = count + 1
print(class_dict)


X = []
y = []
#We'll read every single image as png and we'll create X and y for module that we will have.
for celebrity_name, training_files in celebrity_file_names_dict.items(): #Divide two part (celebrity_name and trainig_files)
    for training_image in training_files: #We got png files
        img = cv2.imread(training_image) #We read it
        if img  is None:
            continue
        scalled_raw_img = cv2.resize(img, (32,32))
        img_har = w2d(img, 'db1',5)
        scalled_img_har = cv2.resize(img_har, (32,32))
        combined_img = np.vstack((scalled_raw_img.reshape(32*32*3,1),scalled_img_har.reshape(32*32,1)))
        X.append(combined_img)
        y.append(class_dict[celebrity_name]) #We can't input celebrity_name to y because it isn't numerical so we created 
                    # a class_dict to convert into numerical
#Convert X  to float 
X = np.array(X).reshape(len(X),4096).astype(float)

#Then we have X and y for classification.


#Modules
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report

X_train, X_test, y_train, y_test = train_test_split(X,y, random_state=0) #TRAIN_TEST_SPLIT

#Note: We will use SVM with rbf kernel tuned with heuristic finetuning.
pipe = Pipeline([('scaler', StandardScaler()) , ('svc', SVC(kernel = 'rbf',C=10))])
 # I'll create a pipeline because i need to get a feel of how my model is performing.
pipe.fit(X_train,y_train)
print(pipe.score(X_test,y_test)) # 0.42857142857142855 score for SVC

#Classification Report is useful to create report matrix.
print(classification_report(y_test, pipe.predict(X_test)))


# We'll create a model and that model tell us which model is best for my project.

#Modules
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV


#That's a model parameters dictionaries.
model_params = {
    'svm' : {
        'model' : svm.SVC(gamma='auto', probability = True),
        'params' : {
            'svc__C': [1,10,100,1000],
            'svc__kernel': ['rbf','linear']
            }
    },
    
    'random_forests' : {
        'model' : RandomForestClassifier(),
        'params' : {
            'randomforestclassifier__n_estimators' : [1,5,10]
            }
    },
    
    'logistic_regression' : {
        'model' : LogisticRegression(solver='liblinear',multi_class='auto'),
        'params' : {
            'logisticregression__C' : [1,5,10]
            }
        }
    }       

#That loop to create a new best estimator to find which algorithms are useful.
scores = []
best_estimators = {}

import pandas as pd
for algo, mp in model_params.items():
    pipe = make_pipeline(StandardScaler(), mp['model'])
    clf = GridSearchCV((pipe), mp['params'], cv = 3, return_train_score=False) #cv = 3 is default mode.
    clf.fit(X_train, y_train)
    scores.append({
        'model' : algo,
        'best_score' : clf.best_score_,
        'best_params' : clf.best_params_
        })
    best_estimators[algo] = clf.best_estimator_

df = pd.DataFrame(scores,columns=['model', 'best_params', 'best_score'])
print(df)    

#Logistic Reg and SVM has same score. 0.586667 (First score)

best_estimators['svm'].score(X_test,y_test) #Out[31]: 0.3333333333333333
best_estimators['random_forests'].score(X_test,y_test) #Out[32]: 0.3333333333333333
best_estimators['logistic_regression'].score(X_test,y_test) #Out[33]: 0.3333333333333333

#The scores that i had is not enough for me but it is private project and i'll make do with it.

best_clf = best_estimators['svm'] #Train Model

#How many samples predicted correctly?
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, best_clf.predict(X_test))

#Let's visualization
import seaborn as sn
plt.figure(figsize = (10,7))
sn.heatmap(cm, annot=True)
plt.xlabel('Predicted')
plt.ylabel('Truth')

print(class_dict)
    
    
import joblib 
# Save the model as a pickle in a file
joblib.dump(best_clf, 'saved_model.pkl')

#Save class dictionary
import json
with open("class_dictionary.json","w") as f:
    f.write(json.dumps(class_dict))






